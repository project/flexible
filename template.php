<?php

function phptemplate_preprocess_page(&$vars) {

   $themepath = path_to_theme();
   $basepath = base_path();
   
   // look for a navigation, content, search links etc
   foreach ($vars as $key=>$value) {
      if (is_string($value) && strpos($value, 'navigation-link')) {
         $vars['navigation_link'] = true;
      }
      if (is_string($value) && strpos($value, 'search-link')) {
         $vars['search_link'] = true;
      }
   }
   
   // is there an accessibility guide?
   $access_guide = theme_get_setting('accessibility_guide_url');
   if ($access_guide && $access_guide != '') {
      $access_guide = l(t('Accessibility guide'), $access_guide);
   } else {
      $access_guide = false;
   }
   $vars['access_guide'] = $access_guide;
   
   // are there additional stylesheets
   $css_sheets = theme_get_setting('css_sheets');
   if ($css_sheets) {
      $css_sheets = explode("\n", $css_sheets);
      foreach ($css_sheets as $sheet) {
         drupal_add_css($themepath.'/'.$sheet, 'theme');
      }
    $vars['styles'] = drupal_get_css();
   }
   
   // is the validator option on?
   if (theme_get_setting('validators')) {
      $vars['validators'] = true;
   }
   
   
   // build the accessibility modes block
   $modes = theme_get_setting('accessibility_modes');
   if ($modes) {
      $modes = explode("\r\n", $modes);
      foreach ($modes as $mode) {
         if ($mode != '') {
            $mode = explode(':', $mode);
            $option = $mode[1];
            $rule = $mode[2];
            $mode = $mode[0];
            $op = substr($rule, 0, 1);
            switch ($op) {
               case '+':
                  $rules[$mode][$option]['add'][] = substr($rule, 1);
                  break;
               case '-':
                  $rules[$mode][$option]['remove'][] = substr($rule, 1);
                  break;
               case '@':
                  $rules[$mode][$option]['image'] = substr($rule, 1);
                  break;
               default:
                  $rules[$mode][$option]['description'] = $rule;
                  break;
            }
         }
      }
      // process the modes - building the output and handling any cookies
      // local copy of the cookies
      $cookie = $_COOKIE;
      // array of css - for possible modification
      $styles = drupal_add_css();
      // index the styles for easy removal
      foreach ($styles as $k0=>$v0) {
         foreach ($v0 as $k1=>$v1) {
            foreach ($v1 as $k2=>$v2) {
               $styles1[$k2][0] = $k0;
               $styles1[$k2][1] = $k1;
            }
         }
      }
      // start to output the mode settings block
      $modes = '<em>'.t('Settings').':</em>';
      $modes1 = '';
      $here = request_uri();
      if (strpos($here, '?q=') === false) {
         $here = '?';
      } else {
         $here = '?q='.$_REQUEST['q'].'&';
      }
      foreach ($rules as $mode=>$options) {
         // any changes to the cookies?
         if (isset($_REQUEST['set_'.$mode])) {
            $cookie[$mode] = $_REQUEST['set_'.$mode];
            $modes1 .= 'Mode changed: '.$mode.'='.$cookie[$mode]."\r";
            setcookie($mode, $cookie[$mode], time() + 86400*365, '/');
         }
         // output the mode options
         $modes .= ' <div class="mode">'.t($mode).'';
         foreach ($options as $option=>$optiondata) {
            $modes .=' <div class="option"><a href="'.$here.'set_'.$mode.'='.$option.'&amp;rand='.rand().'"><img src="'.$basepath.$themepath.'/'.$optiondata['image'].'" alt="'.$optiondata['description'].'" title="'.$optiondata['description'].'" /></a></div>';
         }

         // if the cookie is set for this mode, process the rules
         if (isset($cookie[$mode]) && $cookie[$mode] != '') {
            $option = $cookie[$mode];
            $modes1 .= 'Mode: '.$mode.'='.$option."\r";
            // add any stylesheets
            if (isset($rules[$mode][$option]['add'])) {
               foreach ($rules[$mode][$option]['add'] as $sheet) {
                  $styles['all']['theme'][$themepath.'/'.$sheet] = 1;
                  $modes1 .= 'added: '.$mode.'='.$option.' '.$sheet."\r";
               }
            }
            // remove any stylesheets
            if (isset($rules[$mode][$option]['remove'])) {
               foreach ($rules[$mode][$option]['remove'] as $sheet) {
                  $k0 = $styles1[$sheet][0];
                  $k1 = $styles1[$sheet][1];
                  unset($styles['all']['theme'][$sheet]);
                  $modes1 .= 'removed: '.$mode.'='.$option.' '.$sheet."\r";
               }
            }
         }
         $modes .= '</div>';
      }
      // update the page output for the mode block and the changed styles
      $vars['modes'] = $modes."<!-- \r".$modes1.' -->';
      $vars['styles'] = drupal_get_css($styles);
      $vars['modelocation'] = theme_get_setting('accessibility_modelocation');
      
   }
}

function phptemplate_preprocess_block(&$vars) {

}

function phptemplate_preprocess_node(&$vars) {
}
