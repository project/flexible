<?php

function phptemplate_settings($saved_settings) {
   // additional variables for this theme - the order in which elements appear
   $defaults = array(
      'accessibility_modes'=>'Page:Normal:Normal
Page:Normal:@styles/page_normal.png
Page:Plain:Plain layout without fancy styles
Page:Plain:@styles/page_plain.png
Page:Plain:-sites/all/themes/flexible/flexible2.css
Page:Plain:+styles/page_plain.css

Font:Normal:Normal
Font:Normal:@styles/font_normal.png
Font:Large:Large fonts
Font:Large:@styles/font_large.png
Font:Large:+styles/font_large.css
Font:VeryLarge:Very large fonts
Font:VeryLarge:@styles/font_verylarge.png
Font:VeryLarge:-sites/all/themes/flexible/flexible2.css
Font:VeryLarge:+styles/page_plain.css
Font:VeryLarge:+styles/font_verylarge.css

Colour:Normal:Normal
Colour:Normal:@styles/colour_normal.png
Colour:Restful:Restful colour scheme
Colour:Restful:@styles/colour_restful.png
Colour:Restful:-sites/all/themes/flexible/flexible2_box.css
Colour:Restful:+styles/colour_restful.css
Colour:HighContrast:Light text on a dark background
Colour:HighContrast:@styles/colour_highcontrast.png
Colour:HighContrast:-sites/all/themes/flexible/flexible2_box.css
Colour:HighContrast:+styles/colour_highcontrast.css
',
      'accessibility_modelocation'=>'accessibility',
      'css_header_outer'=>null,
      'css_header_accessibility'=>null,
      'css_header_top'=>null,
      'css_header_inner'=>'box_shadow',
      'css_logo'=>null,
      'css_title'=>null,
      'css_slogan'=>null,
      'css_header_bottom'=>null,
      'css_content_outer'=>null,
      'css_left'=>'box_shadow',
      'css_centre'=>'box_shadow',
      'css_content_top'=>null,
      'css_content_inner'=>null,
      'css_mission'=>null,
      'css_breadcrumb'=>null,
      'css_title'=>null,
      'css_messages'=>null,
      'css_tabs'=>null,
      'css_help'=>null,
      'css_content'=>null,
      'css_contentbottom'=>null,
      'css_right'=>'box_shadow',
      'css_footer_outer'=>'box_shadow',
      'css_footer_top'=>null,
      'css_footer_message'=>null,
      'css_footer_bottom'=>null,
      'no_promoted_content'=>false,
   );

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);
  
  // list of weight options
  $weights = array();
  for ($i = -10; $i < 11; $i++) {
	$weights[$i] = $i;
  }

  // Create the form widgets using Forms API
  $form['accessibility'] = array(
	'#type' => 'fieldset',
	'#title' => t('Accessibility features'),
	'#description' => t('Allows you to set additional accessibility features')
  );
  $form['accessibility']['accessibility_guide_url'] = array(
	'#type' => 'textfield',
	'#title' => t('URL of accessibility guide'),
	'#default_value' => $settings['accessibility_guide_url'],
  );
  $form['accessibility']['accessibility_modes'] = array(
	'#type' => 'textarea',
	'#title' => t('Rules to control accessibility modes'),
	'#default_value' => $settings['accessibility_modes'],
   '#description'=>'Accessibility modes let individual users control aspects of how the website is displayed by switching on or off particular stylesheets.  The default modes are: Site, Font and Colour.  Each mode then has a number of options, for example, the options for Font are Large and Very Large.  Each option can switch on additional CSS styles sheets (or it can switch off existing style sheets).  There should be one rule per line and each rule can be one of the following three types:<pre>Mode:Option:Description {provides a description of the option}<br />Mode:Option:@image {specifies the image that will be displayed to represent this option}<br />Mode:Option:+Stylesheet {applies the given stylesheet if the option is turned on}<br />Mode:Option:-Stylesheet {disables an existing stylesheet if the option is turned on}</pre>Note: When adding a stylesheet the file/path is relative to the theme directory - when removing a stylesheet the full path (without the initial /) must be given, eg:<pre>Page:Plain:-sites/all/themes/flexible2/flexible2.css {disable the main theme stylesheet}<br />Page:Plain:+styles/page_plain.css {add a stylesheet which is in a subdirectory of the theme}</pre>',
  );
  $form['accessibility']['accessibility_modelocation'] = array(
	'#type' => 'select',
	'#title' => t('Where to put the accessibility mode selector'),
   '#description' => t('The accessibility modes are displayed as a set of "icons" - You can decide where to put these icons using this selector.  Note, if you put your icons in the left or right sidebars, you will need to put some other content into those areas to make sure they are displayed properly.'),
	'#default_value' => $settings['accessibility_modelocation'],
   '#options' => array(
      'accessibility'=>'accessibility',
      'left'=>'left',
      'centre'=>'centre',
      'right'=>'right',
      'footer'=>'footer'
   )
  );
  $form['css'] = array(
	'#type' => 'fieldset',
	'#title' => t('CSS classes'),
	'#description' => t("Allows you to set CSS class(es) for specific regions.  Pre-set classes which apply rounded boxes are documented in the <a href=\"/@path\" target=\"_blank\">boxes.htm</a> file which accompanies this theme.  The readme.txt file, which accompanies this theme, contains additional instructions for creating your own box styles.", array('@path'=>path_to_theme().'/boxes.htm')).' '.t('The regions are laid out in the following format:') .
   '<div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: grey; color: white;">body_outer
   <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; color: white; background-color: blue;">header_outer
      <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">accessibility</div>
      <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">header_top</div>
      <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">header_inner
         <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; color: white; background-color: blue;">logo</div>
         <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; color: white; background-color: blue;">title</div>
         <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; color: white; background-color: blue;">slogan</div>
      </div>
      <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">header_bottom</div>
   </div>
   <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; color: white; background-color: blue;">content_outer
      <div style="float: left; width: 15%; height: 15em; margin-right: 10px; padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">left</div>
      <div style="float: left; width: 50%; height: 15em; margin-right: 10px; padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">centre
         <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; color: white; background-color: blue;">content_top</div>
         <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; color: white; background-color: blue;">content_inner
            <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">mission</div>
            <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">breadcrumb</div>
            <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">title</div>
            <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">messages</div>
            <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">tabs</div>
            <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">help</div>
            <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">content</div>
         </div>
         <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; color: white; background-color: blue;">content_bottom</div>
      </div>
      <div style="float: left; width: 15%; height: 15em; padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">right</div>
      <div style="clear: both"></div>
   </div>
   <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; color: white; background-color: blue;">footer_outer
      <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">footer_top</div>
      <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">footer_message</div>
      <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 1px; background-color: white; color: blue;">footer_bottom</div>
   </div>
   </div>'
 
  );
  $form['css']['css_body_outer'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for body_outer section'),
	'#default_value' => $settings['css_body_outer'],
  );
  $form['css']['css_header_outer'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for header_outer section'),
	'#default_value' => $settings['css_header_outer'],
  );
  $form['css']['css_accessibility'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for accessibility section'),
	'#default_value' => $settings['css_accessibility'],
  );
  $form['css']['css_header_top'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for header_top section'),
	'#default_value' => $settings['css_header_top'],
  );
  $form['css']['css_header_inner'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for header_inner section'),
	'#default_value' => $settings['css_header_inner'],
  );
  $form['css']['css_logo'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for logo section'),
	'#default_value' => $settings['css_logo'],
  );
  $form['css']['css_site_name'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for site_name section'),
	'#default_value' => $settings['css_site_name'],
  );
  $form['css']['css_site_slogan'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for site_slogan section'),
	'#default_value' => $settings['css_site_slogan'],
  );
  $form['css']['css_header_bottom'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for header_bottom section'),
	'#default_value' => $settings['css_header_bottom'],
  );
  $form['css']['css_content_outer'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for content_outer section'),
	'#default_value' => $settings['css_content_outer'],
  );
  $form['css']['css_left'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for left section'),
	'#default_value' => $settings['css_left'],
  );
  $form['css']['css_centre'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for centre section'),
	'#default_value' => $settings['css_centre'],
  );
  $form['css']['css_content_top'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for content_top section'),
	'#default_value' => $settings['css_content_top'],
  );
  $form['css']['css_content_inner'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for content_inner section'),
	'#default_value' => $settings['css_content_inner'],
  );
  $form['css']['css_mission'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for mission section'),
	'#default_value' => $settings['css_mission'],
  );
  $form['css']['css_breadcrumb'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for breadcrumb section'),
	'#default_value' => $settings['css_breadcrumb'],
  );
  $form['css']['css_title'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for title section'),
	'#default_value' => $settings['css_title'],
  );
  $form['css']['css_messages'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for messages section'),
	'#default_value' => $settings['css_messages'],
  );
  $form['css']['css_tabs'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for tabs section'),
	'#default_value' => $settings['css_tabs'],
  );
  $form['css']['css_help'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for help section'),
	'#default_value' => $settings['css_help'],
  );
  $form['css']['css_content'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for content section'),
	'#default_value' => $settings['css_content'],
  );
  $form['css']['css_content_bottom'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for content_bottom section'),
	'#default_value' => $settings['css_content_bottom'],
  );
  $form['css']['css_right'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for right section'),
	'#default_value' => $settings['css_right'],
  );
  $form['css']['css_footer_outer'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for footer_outer section'),
	'#default_value' => $settings['css_footer_outer'],
  );
  $form['css']['css_footer_top'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for footer_top section'),
	'#default_value' => $settings['css_footer_top'],
  );
  $form['css']['css_footer_message'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for footer_message section'),
	'#default_value' => $settings['css_footer_message'],
  );
  $form['css']['css_footer_bottom'] = array(
	'#type' => 'textfield',
	'#title' => t('CSS class(es) for footer_bottom section'),
	'#default_value' => $settings['css_footer_bottom'],
  );
  $form['css']['blocks'] = array(
	'#value' => t('You can also apply CSS classes to specific blocks delivered from Drupal core.  Blocks are usually identified by a module name and a delta value - for instance the Navigation links come from the "user" module and the delta is "1", i.e. "user-1"; Primary links are identified as "menu-primary-links".  etc.'),
  );
  $form['css']['block_id'] = array(
	'#type' => 'checkbox',
	'#title' => t('Identify Drupal blocks'),
	'#default_value' => $settings['block_id'],
   '#description' => t('Turn this feature on to help you identify the Drupal block id during the development of your site - don\'t forget to turn it off for production sites!')
  );
  $form['css']['block_rules'] = array(
	'#type' => 'textarea',
	'#title' => t('CSS rules for blocks'),
	'#default_value' => $settings['block_rules'],
   '#description' => t('Enter the rules for applying CSS classes to specific blocks here.  Each rule should be on a separate line and should be of the form: module-delta: classes.  For example, if you want the navigation block to have a CSS class of MyClass, you should enter:<pre>user-1:MyClass</pre> or, to put a shadow box around the primary links block<pre>menu-primary-links: box_shadow</pre>')
  );
  $form['css']['info'] = array(
   '#value'=> t('<strong>Body classes and Page-specific classes:</strong><br />The theme will put the Drupal standard body classes on the body element - these give you the opportunity to tailor the CSS according to the type of page.  In addition, the theme will also tell you the actual page, eg if the page is ?q=node/192 the body will have a class of page_node_192 (ie page_ followed by the path of the page where / is changed to _).  This will let you have specific CSS rules for specific pages.')
  );
  $form['css']['css_rules'] = array(
	'#type' => 'textarea',
	'#title' => t('Additional inline CSS'),
	'#default_value' => $settings['css_rules'],
   '#description' => t('This section lets you type CSS rules which will be inserted, as an inline style, into every page.  This isn\'t the most efficient way to deliver CSS to the browser (it isn\'t cached) so it is suggested that you only use it for prototyping or very small "tweaks".  One you have worked out the CSS you need you should put it into custom.css or one of the additional css stylesheets, which you can specify in the next section.')
  );
  $form['css']['css_sheets'] = array(
	'#type' => 'textarea',
	'#title' => t('Additional CSS sheets'),
	'#default_value' => $settings['css_sheets'],
   '#description' => t('If you want your own stylesheets you can list them here.  Put each stylesheet name on a separate line.  The system will look for the stylesheet in the same directory as the Flexible 2 theme - if you put the stylesheet in a subdirectory, you will need to specify that as well eg<pre>MySubdirectory/MySheet.css</pre>  Note, you might also want to switch off these style sheets if a user selects one of the accessibility modes.  So, if you added mystyles.css in the theme directory, you can disable it whenever High Contrast mode is selected by adding the following line to the "Rules to control accessibility modes:" section: <pre>Colour:HighContrast:-sites/all/themes/flexible/mystyles.css</pre> ')
  );
  $form['css']['validators'] = array(
	'#type' => 'checkbox',
	'#title' => t('Show links to validators'),
	'#default_value' => $settings['validators'],
   '#description' => t('Turn this feature on to display links to the W3C validators - you will probably only want to enable this feature during development.')
  );
  $form['other'] = array(
	'#type' => 'fieldset',
	'#title' => t('Other settings'),
  );
  $form['other']['no_promoted_content'] = array(
	'#type' => 'checkbox',
	'#title' => t('No promoted front page content'),
	'#default_value' => $settings['no_promoted_content'],
   '#description' => t('If you check this setting, Drupal will not put its default content on the front page, ie "node".  If you don\'t have any promoted nodes, this setting will prevent the "Welcome to Drupal" message being displayed.  If you do have promoted nodes, they will not be displayed.')
  );
  
  // Return the additional form widgets
  return $form;
}	
