Flexible 2
==========
Flexible 2 is a Drupal 6 compatible replacement for the Flexible theme.  It is a complete rewite, hence the change of name.  Key features are:
   - Accessibility navigation links at start of page
   - Pre and post header regions to give administrators flexibility as to where to place their content
   - Table-less two or three column layout
   - Accessibility settings for:
      - - Switching page layout from 2/3 column to linear
      - - Change font sizes
      - - Change colour schemes
   - Some pre-set stylesheets, to allow content to be wrapped in boxes with rounded corners
   
(Accessibility keys, which were implemented in the Drupal 5 version of the Flexible theme, have not been included in this version.)

Theme settings
==============
You need to visit the theme settings page, admin/build/themes/settings/flexible2, after enabling the theme to switch on some of the default settings.  You can also use the settings screen to make your own customisations.

The theme settings page has a number of options allowing you to control the layout:
   - Accessibility settings:
      - - URL of accessibility guide - Lets you specify a URL (http://...) or a Drupal node (node/..) which contains the site's accessibility guide.  If specified, this will be displayed at the top of the page in the accessibility links section.
      - - Rules to control the accessibility modes - for each mode you can specify which stylesheets to turn on (or off)
      - - Where to put the accessibility mode selector on the screen
   - Standard settings:
      - - CSS classes for each of the sections of the page.  You can specify the CSS classes for each of these sections, which will allow you to introduce pre-written styling information for these sections.  For instance, setting the class as "box_shadow" will apply rounded corners and a shadow effect to the section.  Pre-set classes supplied with this theme are documented in the accompanying file, boxes.htm
      - - Rules, which allow you to add CSS classes to specific blocks of content.  As with the main sections of the page, these can be used to give blocks, such as navigation or primary links, one of the pre-written rounded box styles.  To help you identify the Drupal block names a feature can be switched which will display these.
      - - Inline CSS allows you to add inline CSS rules to your page
      - - You can also specify additional style sheets, which you can add to your page.
      - - Show links to W3C validators so you can validate your HTML and CSS with a single click.
   

Page layout
===========
The following shows the layout of the page and how the elements are identified (the settings screen also has a diagram of the page layout):

   SECTION(header_outer)
      SECTION(accessibility): dynamically-generated accessibility links, eg "skip to content", "skip to navigation" etc
      REGION(header_top): block elements that you wish to place before the logo, title etc
      SECTION(header_inner)
         ELEMENT(logo)
         ELEMENT(title)
         ELEMENT(slogan)
      REGION(header_bottom): block elements that you wish to place after the logo, title etc
         
   SECTION(content_outer)
      REGION(left): block elements which you wish to place in the left hand column
      SECTION(centre)
            REGION(content_top): block elements which you wish to place before the main page content
            SECTION(content_inner)
               ELEMENT(mission): the mission statement, which only appears on the front page
               ITEM(breadcrumb): the breadcrumb trail
               ITEM(title): the page title
               ITEM(messages): any messages supplied by Drupal
               ITEM(tabs): sub-menu items, which are rendered in tabs
               ITEM(help): any help text for the current page
               ITEM(content): the main page content
            REGION(content_bottom): block elements which you wish to place before the main page content
      REGION(right): block elements which you wish to place in the right hand column

   SECTION(footer_outer)
      REGION(footer_top)
      ELEMENT(footer_message)
      REGION(footer_bottom)
      
All the SECTIONS, REGIONS, ELEMENTS AND ITEMS shown above have the following features:

   - They are wrapped in a DIV tag of the same id, eg <div id="header_outer">...</div>.

   - They have their contents wrapped in a set of 10 DIv tags.  Additional CSS, such as padding, can be applied to any of these.  A specific use for these tags is to supply background images to make the content appear in a box with rounded corners.
      <div class="b0">
      <div class="b1">
      <div class="b2">
      <div class="b3">
      <div class="b4">
      <div class="b5">
      <div class="b6">
      <div class="b7">
      <div class="b8">
      <div class="b9">
   - They can be assigned arbitrary class names using the theme adminstration facility: Home � Administer � Site building � Themes � Configure.  This feature can be used to invoke pre-defined styles, such as box_shadow.
   
SECTIONs determine the basic page layout.  They are determined by page.tpl.php.

REGIONs correspond to the regions provided by the theme.  The content of each region is determined by Administer � Site buiding � Blocks, which lets you position blocks of content in each region.

ELEMENTs contain information about your site.  Their values can be set using Administer � Site configuration � Site information.

ITEMs are set dynamically by Drupal, depending on what is being displayed

Items not included
==================
Some themes directly implement elements such as:
   search_box
   primary_links
   secondary_links
   feed_icons
   
To enable these items in this theme you need to position them in one of the pre-defined regions using Administer � Site buiding � Blocks.  This gives you the flexibility to put the items where you want them (and only if you want them).

Creating boxes
==============
To create a new box layout the following procedure is suggested, though it is, of course, possible to adopt a different approach.

To create a box layout called box_fred:
 - Create an image using graphics software (suggested size: 90x90 pixels)
 - Draw the required box on the image (suggest that the edge of the box is 10 pixels from the edge of the image)
 - Slice the image into 9 sections, 3 rows and 3 columns, each 30x30 pixels.  These slices will makes up the corners, top, bottom and sides of the box.  The slices for the top, bottom and sides will be repeated as necessary to fill the box, so your boxes will dynamically resize to fit any content.  Suggested names for the sliced images are:
      box_1x1.png    box_1x2.png    box_1x3.png
      box_2x1.png    box_2x2.png    box_2x3.png
      box_3x1.png    box_3x2.png    box_3x3.png
 - Saved the image slices into a subdirectory of the theme called box_fred
 - Paste the following CSS into custom.css, adapting it as necessary.  Note the use of > to denote immediate child elements - this is to prevent nested boxes inheriting the style of their parents
      /*
      ========
      box_fred
      --------
      */
      div.box_fred 
         {border-bottom: none;}
      div.box_fred > div.b0 
         {background-image: url(box_fred/box_1x2.png); background-position: top; background-repeat: repeat-x;}
      div.box_fred > div.b0 > div.b1 
         {background-image: url(box_fred/box_2x3.png); background-position: right; background-repeat: repeat-y;}
      div.box_fred > div.b0 > div.b1 > div.b2 
         {background-image: url(box_fred/box_3x2.png); background-position: bottom; background-repeat: repeat-x;}
      div.box_fred > div.b0 > div.b1 > div.b2 > div.b3 
         {background-image: url(box_fred/box_2x1.png); background-position: left; background-repeat: repeat-y;}
      div.box_fred > div.b0 > div.b1 > div.b2 > div.b3 > div.b4 
         {background-image: url(box_fred/box_1x1.png); background-position: top left; background-repeat: no-repeat;}
      div.box_fred > div.b0 > div.b1 > div.b2 > div.b3 > div.b4 > div.b5 
         {background-image: url(box_fred/box_1x3.png); background-position: top right; background-repeat: no-repeat;}
      div.box_fred > div.b0 > div.b1 > div.b2 > div.b3 > div.b4 > div.b5 > div.b6 
         {background-image: url(box_fred/box_3x3.png); background-position: bottom right; background-repeat: no-repeat;}
      div.box_fred > div.b0 > div.b1 > div.b2 > div.b3 > div.b4 > div.b5 > div.b6 > div.b7 
         {background-image: url(box_fred/box_3x1.png); background-position: bottom left; background-repeat: no-repeat;}
      div.box_fred > div.b0 > div.b1 > div.b2 > div.b3 > div.b4 > div.b5 > div.b6 > div.b7 > div.b8 > div.b9 
         {padding-top: 15px; padding-left: 15px; padding-right: 25px; padding-bottom: 25px;}
