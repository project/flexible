<?php 
   /* Flexible 2 page template
   */
   
   $boxstart = '<div class="b0"><div class="b1"><div class="b2"><div class="b3"><div class="b4"><div class="b5"><div class="b6"><div class="b7"><div class="b8"><div class="b9">';
   $boxend = '</div></div></div></div></div></div></div></div></div></div>';
   
  $css_rules = theme_get_setting('css_rules');
   
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">

<head>
   <title><?php print $head_title; ?></title>
   <?php print $head; ?>
   <?php print $styles; ?>
   <?php print $scripts; ?>
   <?php if ($css_rules): ?>
      <style type="text/css">
         <?php print $css_rules ?>
      </style>
   <?php endif; ?>
</head>

<body class="<?php print $body_classes; ?> path_<?php print str_replace('/','_',drupal_get_path_alias($_GET['q'])) ?>">
<div id="body_outer" class="<?php print theme_get_setting('css_body_outer') ?>"><?php print $boxstart; ?>
   <!-- HEADER OUTER -->
   <div id="header_outer" class="<?php print theme_get_setting('css_header_outer') ?>"><?php print $boxstart; ?>
      <!-- ACCESSIBILITY -->
      <a id="top"></a>
      <div id="accessibility" class="<?php print theme_get_setting('css_accessibility') ?>"><?php print $boxstart; ?>
         <ul>
            <li><a href="#content-link"><?php print t('Skip to content') ?></a></li>
            <?php if ($navigation_link): ?>
               <li><a href="#navigation-link"><?php print t('Skip to navigation') ?></a></li>
            <?php endif; ?>
            <?php if ($search_link): ?>
               <li><a href="#search-link"><?php print t('Skip to search') ?></a></li>
            <?php endif; ?>
            <?php if ($access_guide): ?>
               <li><?php print $access_guide; ?></li>
            <?php endif; ?>
         </ul>
         <?php if ($modes && ($modelocation == 'accessibility')): ?>
            <div class="modes"> - <?php print $modes; ?></div>
         <?php endif; ?>
      <?php print $boxend; ?></div>
   
      <!-- HEADER TOP -->
      <?php if ($header_top): ?>
         <div id="header_top" class="<?php print theme_get_setting('css_header_top') ?>"><?php print $boxstart; ?>
            <?php print $header_top ?>
         <?php print $boxend; ?></div>
      <?php endif; ?>
   
      <!-- HEADER INNER -->
      <?php if ($logo || $site_name || $site_slogan): ?>
         <div id="header_inner" class="<?php print theme_get_setting('css_header_inner') ?>"><?php print $boxstart; ?>
            <!-- LOGO -->
            <?php if ($logo): ?>
               <div id="logo" class="<?php print theme_get_setting('css_logo') ?>"><?php print $boxstart; ?>
                  <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo-image" /></a>
               <?php print $boxend; ?></div>
            <?php endif; ?>
            <!-- SITE NAME -->
            <?php if ($site_name): ?>
               <div id="site_name" class="<?php print theme_get_setting('css_site_name') ?>"><?php print $boxstart; ?>
                  <h1><a href="<?php print $base_path; ?>" title="<?php print $site_name; ?>"><?php print $site_name; ?></a></h1>
               <?php print $boxend; ?></div>
            <?php endif; ?>
            <!-- SITE SLOGAN -->
            <?php if ($site_slogan): ?>
               <div id="site_slogan" class="<?php print theme_get_setting('css_site_slogan') ?>"><?php print $boxstart; ?>
                  <?php print $site_slogan; ?>
               <?php print $boxend; ?></div>
            <?php endif; ?>
         <?php print $boxend; ?></div>
      <?php endif; ?>
      
      <!-- HEADER BOTTOM -->
      <?php if ($header_bottom): ?>
         <div id="header_bottom" class="<?php print theme_get_setting('css_header_bottom') ?>"><?php print $boxstart; ?>
            <?php print $header_bottom ?>
         <?php print $boxend; ?></div>
      <?php endif; ?>
   <?php print $boxend; ?></div>
   
   <!-- CONTENT OUTER -->
   <div id="content_outer" class="<?php print theme_get_setting('css_content_outer') ?>"><?php print $boxstart; ?>
      <!-- LEFT -->
      <?php if ($left || ($modes && ($modelocation == 'left'))): ?>
         <div id="left" class="<?php print theme_get_setting('css_left') ?>"><?php print $boxstart; ?>
            <?php if ($modes && ($modelocation == 'left')): ?>
               <div class="modes"><?php print $modes; ?></div>
            <?php endif; ?>
            <?php print $left ?>
         <?php print $boxend; ?></div>
      <?php endif; ?>

      <!-- CENTRE -->
      <div id="centre" class="<?php print theme_get_setting('css_centre') ?>"><?php print $boxstart; ?>

         <?php if ($modes && ($modelocation == 'centre')): ?>
            <div class="modes"><?php print $modes; ?></div>
         <?php endif; ?>

         <!-- CONTENT TOP -->
         <?php if ($content_top): ?>
            <div id="content_top" class="<?php print theme_get_setting('css_content_top') ?>"><?php print $boxstart; ?>
               <?php print $content_top ?>
            <?php print $boxend; ?></div>
         <?php endif; ?>

         <!-- CONTENT INNER -->
         <?php if ($mission || $breadcrumb || $title || $messages || $tabs || $help || $content): ?>
            <div id="content_inner" class="<?php print theme_get_setting('css_content_inner') ?>"><?php print $boxstart; ?>
               <!-- MISSION -->
               <?php if ($mission): ?>
                  <div id="mission" class="<?php print theme_get_setting('css_mission') ?>"><?php print $boxstart; ?>
                     <?php print $mission ?>
                  <?php print $boxend; ?></div>
               <?php endif; ?>
               <!-- BREADCRUMB -->
               <?php if ($breadcrumb): ?>
                  <div id="breadcrumb" class="<?php print theme_get_setting('css_breadcrumb') ?>"><?php print $boxstart; ?>
                     <?php print $breadcrumb ?>
                  <?php print $boxend; ?></div>
               <?php endif; ?>
               <!-- CONTENT LINK -->
               <a id="content-link" ></a>
               <a class="top-link" href="#top"><?php print ('Skip to top of page'); ?></a>
               <!-- TITLE -->
               <?php if ($title): ?>
                  <div id="title" class="<?php print theme_get_setting('css_title') ?>"><?php print $boxstart; ?>
                     <h2><?php print $title ?></h2>
                  <?php print $boxend; ?></div>
               <?php endif; ?>
               <!-- MESSAGES -->
               <?php if ($messages): ?>
                  <div id="messages" class="<?php print theme_get_setting('css_messages') ?>"><?php print $boxstart; ?>
                     <?php print $messages ?>
                  <?php print $boxend; ?></div>
               <?php endif; ?>
               <!-- TABS -->
               <?php if ($tabs): ?>
                  <div id="tabs" class="<?php print theme_get_setting('css_tabs') ?>"><?php print $boxstart; ?>
                     <?php print $tabs ?>
                  <?php print $boxend; ?></div>
               <?php endif; ?>
               <!-- HELP -->
               <?php if ($help): ?>
                  <div id="help" class="<?php print theme_get_setting('css_help') ?>"><?php print $boxstart; ?>
                     <?php print $help ?>
                  <?php print $boxend; ?></div>
               <?php endif; ?>

               <!-- CONTENT -->
               <?php if ($content): ?>
                  <div id="content" class="<?php print theme_get_setting('css_content') ?>"><?php print $boxstart; ?>
                     <?php if (!drupal_is_front_page() || !theme_get_setting('no_promoted_content')) { ?>
                        <?php print $content; ?>
                     <?php } ?>
                  <?php print $boxend; ?></div>
               <?php endif; ?>

            <?php print $boxend; ?></div>
         <?php endif; ?>
   
         <!-- CONTENT BOTTOM -->
         <?php if ($content_bottom): ?>
            <div id="content_bottom" class="<?php print theme_get_setting('css_content_bottom') ?>"><?php print $boxstart; ?>
               <?php print $content_bottom ?>
            <?php print $boxend; ?></div>
         <?php endif; ?>
      
      <?php print $boxend; ?></div>
      
      <!-- RIGHT -->
      <?php if ($right || (modes && ($modelocation == 'right'))): ?>
         <div id="right" class="<?php print theme_get_setting('css_right') ?>"><?php print $boxstart; ?>
            <?php if ($modes && ($modelocation == 'right')): ?>
               <div class="modes"><?php print $modes; ?></div>
            <?php endif; ?>
            <?php print $right ?>
         <?php print $boxend; ?></div>
      <?php endif; ?>
   
      <!-- need this for the parent div to be resized correctly in Firefox -->
      <div class="clearboth"></div>

   <?php print $boxend; ?></div>
   
   <!-- FOOTER -->
   <?php if ($footer_top || $footer_message || $footer_bottom || ($modelocation == 'footer')): ?>
      <div id="footer_outer" class="<?php print theme_get_setting('css_footer_outer') ?>"><?php print $boxstart; ?>
         <!-- FOOTER TOP -->
         <?php if ($footer_top): ?>
            <div id="footer_top" class="<?php print theme_get_setting('css_footer_top') ?>"><?php print $boxstart; ?>
               <?php print $footer_top ?>
            <?php print $boxend; ?></div>
         <?php endif; ?>
         <!-- FOOTER MESSAGE -->
         <?php if ($footer_message): ?>
            <div id="footer_message" class="<?php print theme_get_setting('css_footer_message') ?>"><?php print $boxstart; ?>
               <?php print $footer_message ?>
            <?php print $boxend; ?></div>
         <?php endif; ?>
         <!-- FOOTER BOTTOM -->
         <?php if ($footer_bottom): ?>
            <div id="footer_bottom" class="<?php print theme_get_setting('css_footer_bottom') ?>"><?php print $boxstart; ?>
               <?php print $footer_bottom ?>
            <?php print $boxend; ?></div>
         <?php endif; ?>
         <?php if ($modes && ($modelocation == 'footer')): ?>
            <div class="modes"><?php print $modes; ?></div>
         <?php endif; ?>
      <?php print $boxend; ?></div>
   <?php endif; ?>

<?php print $boxend; ?></div>
<?php if ($validators): 
   $uri = 'xxxxx'; ?>
   <div class="validators">
    <a href="http://validator.w3.org/check?uri=referer"><img
        src="http://www.w3.org/Icons/valid-xhtml10-blue"
        alt="Valid XHTML 1.0 Strict" height="31" width="88" /></a>
    <a href="http://jigsaw.w3.org/css-validator/check/referer">
        <img style="border:0;width:88px;height:31px"
            src="http://jigsaw.w3.org/css-validator/images/vcss-blue"
            alt="Valid CSS!" />
    </a>
   </div>
<?php endif; ?>

<?php print $closure; ?> 
</body>

</html>
