<?php
   /* 
      Redefinition of block content
      Needed so that a navigation link can be inserted into the navigation block (block=user, delta = 1)
   */
   
   // get the block id
   $block_id = 'block<br />module: '.$block->module .'<br />delta:'. $block->delta;
   // show the block id
   if (theme_get_setting('block_id')) {
      $show_block_id = '<span style="background-color: blue; color: white;">'.$block_id.'</span>';
   } else {
      $show_block_id = '';
   }
   // get the rules
   $block_id = $block->module .'-'. $block->delta;
   $block_rules = "\n".theme_get_setting('block_rules');
   // is there a rule?
   $pos = strpos($block_rules, $block_id);
   if ($pos) {
      $pos += strlen($block_id);
      $block_rules = substr($block_rules, $pos + 1);
      $rule = explode("\r", $block_rules);
      $rule = $rule[0];
      $boxstart = '<div class="b0"><div class="b1"><div class="b2"><div class="b3"><div class="b4"><div class="b5"><div class="b6"><div class="b7"><div class="b8"><div class="b9">';
      $boxend = '</div></div></div></div></div></div></div></div></div></div>';

   } else {
      $rule = '';
   }
   
?>
<!-- START <?php print $block->module .'-'. $block->delta; ?> -->
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block <?php print $block_classes.' '.$rule; ?>">
   <?php print $boxstart; print $show_block_id; ?>
   <div class="block-inner">
      <?php if ($block->subject): ?>
         <h2 class="title"><?php print $block->subject; ?></h2>
      <?php endif; ?>
      <?php // navigation link on block user-1
      if ($block->module == 'user' &&  $block->delta == '1'): ?>
         <a id="navigation-link" />
         <a class="top-link" href="#top"><?php print ('Skip to top of page'); ?></a>
      <?php endif; ?>
      <?php // search link on block search-0
      if ($block->module == 'search' &&  $block->delta == '0'): ?>
         <a id="search-link" />
         <a class="top-link" href="#top"><?php print ('Skip to top of page'); ?></a>
      <?php endif; ?>
      <div class="content"><?php print $block->content; ?></div>
   </div>
   <?php print $boxend; ?>
</div><!-- END <?php print $block->module .'-'. $block->delta; ?> -->
